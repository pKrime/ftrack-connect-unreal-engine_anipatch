..
    :copyright: Copyright (c) 2014-2020 ftrack

.. _release/release_notes:

*************
Release Notes
*************


.. release:: Upcoming

    .. change:: Fix

        Animation import settings are set by file format.

.. release:: 1.0.1
    :date: 2020-09-10

    .. change:: Fix

        .plugin misses of various fields needed for the marketplace release.

    .. change:: New

        Add integration usage tracking.

.. release:: 1.0.0
    :date: 2020-09-02

    .. change:: Changed

        Review documentation for plugin rebuild procedure.

.. release:: 1.0.0-RC2
    :date: 2020-05-04

    .. change:: Fix

        Disable publish for unsupported asset types.


    .. change:: New

        Add sphinx documentation.



.. release:: 1.0.0-RC1
    :date: 2020-04-24

    .. change:: New

        Ensure publish on any context.

    .. change:: New

        First stable release.
